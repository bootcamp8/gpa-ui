export class Student {
    studentId : number;
    studentName : string;
    address :string;
    grade : number;
    gpa : number;
}