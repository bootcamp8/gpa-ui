import { Component, OnInit } from '@angular/core';
import { Student } from 'src/entity/student.model';
import { StudentService } from 'src/services/student.service';

@Component({
  selector: 'app-student-add',
  templateUrl: './student-add.component.html',
  styleUrls: ['./student-add.component.css']
})
export class StudentAddComponent implements OnInit {

  name : string ;
  address : string ;
  grade : number ;
  student : Student;
  statusMessage : string = "";

  constructor(private studentService : StudentService) { }

  ngOnInit() {
  }

  saveStudent(){

    this.student = new Student();
    this.student.studentName = this.name;
    this.student.address = this.address;
    this.student.grade = this.grade;
    
    this.studentService.addNewStudent(this.student).subscribe(
      response =>{
        this.statusMessage = "Student record saved successfully";
     },
     err =>{
      this.statusMessage = "Error occured while saving records";
     }
    )

  }

}
