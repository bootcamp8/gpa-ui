import { Component, OnInit } from '@angular/core';
import { Student } from 'src/entity/student.model';
import { StudentService } from 'src/services/student.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  studentId : number;
  inputStudent : Student;
  student : Student;
  studentName : string;

  constructor( private studentService:StudentService) { }

  ngOnInit() {
    // this.student = new Student()
  }

  getStudentDetails(){
     console.log("ID : " + this.studentId );
     this.inputStudent = new Student();
     this.inputStudent.studentId = this.studentId;

     this.studentService.getStudentDetails(this.inputStudent).subscribe( response => {
        this.student = response;
      }
     )
  }

}
