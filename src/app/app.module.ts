import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GpaHeaderComponent } from './gpa-header/gpa-header.component';
import { GpaMenuComponent } from './gpa-menu/gpa-menu.component';
import { StudentAddComponent } from './student-add/student-add.component';
import { GpaHomeComponent } from './gpa-home/gpa-home.component';
import { HttpClientModule } from '@angular/common/http';
import { StudentDetailsComponent } from './student-details/student-details.component';

@NgModule({
  declarations: [
    AppComponent,
    GpaHeaderComponent,
    GpaMenuComponent,
    StudentAddComponent,
    GpaHomeComponent,
    StudentDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
