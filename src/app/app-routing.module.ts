import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GpaHomeComponent } from './gpa-home/gpa-home.component';
import { StudentAddComponent } from './student-add/student-add.component';
import { StudentDetailsComponent } from './student-details/student-details.component';


const routes: Routes = [
  { path: 'home', component: GpaHomeComponent },
  { path: 'addStudent', component: StudentAddComponent },
  { path: 'studentDetails', component: StudentDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
