import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Student } from 'src/entity/student.model';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http : HttpClient) { }

  addNewStudent(student:Student){
    console.log("In student service :" + student);
    return this.http.post('http://localhost:8080/gpa/student/add',student);
  }

  getStudentDetails(student:Student){
    return this.http.post<Student>('http://localhost:8080//gpa/student/details',student);
}

}
